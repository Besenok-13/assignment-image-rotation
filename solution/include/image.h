//
// Created by Aerocobra on 21.12.2021.
//

#ifndef ASSIGNMENT_IMAGE_ROTATION_IMAGE_H
#define ASSIGNMENT_IMAGE_ROTATION_IMAGE_H

#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>

#pragma pack(push, 1)
struct pixel { uint8_t b, g, r; };
#pragma pack(pop)

struct image {
    uint64_t width, height;
    struct pixel* data;
};

struct image constructor(const struct image);
void destructor(const struct image);
#endif
