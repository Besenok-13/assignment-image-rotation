
// Created by Aerocobra on 21.12.2021.

#ifndef ASSIGNMENT_IMAGE_ROTATION_ROTATE_H
#define ASSIGNMENT_IMAGE_ROTATION_ROTATE_H

struct image rotate( struct image const image );

#endif
