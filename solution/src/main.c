// Created by Aerocobra on 21.12.2021.

#include "bmp.h"
#include "rotate.h"

#include <stdio.h>

int main( int argc, char** argv ) {
    (void) argc; (void) argv;
    // просто потому, что да, чтобы не поврелить картинку
    // мы используем 2 разделённых потока

    struct image to_rotate; //как ясно из названия - открываем, изображение для поворота
    struct image res; // результат наших адских трудов

    FILE *file = fopen(argv[1], "rb"); // передаём в поток файлик только для считывания, чтобы не повредить файлик
    FILE *output = fopen(argv[2], "wb"); // а тут уже готовимся записывать

    from_bmp(file, &to_rotate); //ДАЙ МНЕ СВОЙ РАЗМЕР И ПОДГОНИ ЕГО ПОД ЗАДАЧУ, СЛУЖИ МНЕ И... а, это не та программа
    fclose(file); //Ухади, поток закрой, у меня теперь другой!

    res = rotate(to_rotate); //Избушка избушка, повернись к лесу на 90 градусов, и ко мне на 90 градусов.

    to_bmp(output, &res); //Я ПЕРЕДАЛАЮ ТЕБЯ ПОД СЕБЯЯЯ (не костыль - а фича, тем более, будет как у всех)

    fclose(output); // Ты друг(нет) тоже уходи, и с собой мои нервы уводи
    destructor(res);

    return 0;
}
