
// Created by Aerocobra on 21.12.2021.

#include "bmp.h"

#include <stdio.h>
#include <stdlib.h>

struct bmp_header;

uint8_t get_padding(const struct image* image) {
    return image->width % 4;
}

struct bmp_header set_header(const struct image image) {
    int32_t height = image.height;
    int32_t width = image.width;
    struct bmp_header header = {
            .bfType = 0x4D42,
            .bfileSize = (sizeof(struct bmp_header) + height * width * sizeof(struct pixel)
                          + height * get_padding(&image)),
            .bfReserved = 0,
            .bOffBits = sizeof(struct bmp_header),
            .biSize = 40,
            .biWidth = width,
            .biHeight = height,
            .biPlanes = 1,
            .biBitCount = 24,
            .biCompression = 0,
            .biSizeImage = height * width * sizeof(struct pixel) + get_padding(&image) * height,
            .biXPelsPerMeter = 0,
            .biYPelsPerMeter = 0,
            .biClrUsed = 0,
            .biClrImportant = 0
    };
    return header;
}


enum read_status from_bmp( FILE* in, struct image* image ){

    struct bmp_header* header = malloc(sizeof(struct bmp_header));

    fread(header, sizeof(struct bmp_header), 1, in);

    if (header->bfType != 0x4D42) {
        return READ_INVALID_SIGNATURE;
    }
    if (header->biSize != 40) {
        return READ_INVALID_HEADER;
    }
    if (header->biBitCount != 24) {
        return READ_INVALID_BITS;
    }


    image->data = malloc(sizeof(struct pixel) * header->biWidth * header->biHeight);

    if(header->biWidth && header->biHeight > 0) { // я вошёл в режим паранои и теперь не уверен, что длина и ширина ВООБЩЕ ЕСТЬ
        image->width = header->biWidth;
        image->height = header->biHeight;
    } else return READ_INVALID_BITS; //Говорим, что человек, который кинул картинку с 0 пиксилей - чорт

    for(uint32_t i = 0; i < header->biHeight; i++) {
        fread(&(image->data[i * image->width]), sizeof(struct pixel), header->biWidth, in);
        fseek( in, get_padding(image), SEEK_CUR);
    }
    free(header);
    return READ_OK;
}

enum write_status to_bmp( FILE* out, struct image const* image ) {
    struct bmp_header header = set_header(*image);

    fwrite(&header, sizeof(struct bmp_header), 1, out);


    const size_t padding = 0;

    for(uint32_t h = 0; h < image->height; h++) {
        fwrite(&(image->data[h * image->width]), sizeof(struct pixel), image->width, out);
        fwrite(&padding, 1, get_padding(image), out);
    }

    return WRITE_OK;
}
