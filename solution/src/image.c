
// Created by Aerocobra on 21.12.2021.

// Вот эта штука мне нравится)))
#include "image.h"

struct image;

struct image constructor(const struct image image) {
    return (struct image) {.height = image.width, .width = image.height};
}

void destructor(const struct image image) {
    free(image.data);
}
